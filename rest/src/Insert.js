import React from 'react';
import axios from 'axios';
import Button from 'react-bootstrap/lib/Button';
import FormControl from 'react-bootstrap/lib/FormControl';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import Label from 'react-bootstrap/lib/Label';


class Insert extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            usuario: '',
            password: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }


    handleChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }


    handleSubmit(event) {
        console.log('El nombre ingresado es: ' + this.state.usuario + " / pass: " + this.state.password);
        event.preventDefault();
        axios.post('https://api-zendesk.herokuapp.com', {
            usuario: this.state.usuario,
            password: this.state.password
        }).then((response) => {
            console.log(response.data);
            window.location.href = "http://localhost:3000/#/";
        }).catch(function (error) {
            console.log(error)
        });
    }

    getValidationState() {
        const length = this.state.usuario.length;
        if (length > 10) return 'success';
        else if (length > 5) return 'warning';
        else if (length > 0) return 'error';
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <FormGroup controlId="formBasicText" validationState={this.getValidationState()}>
                    <Label>Name</Label>
                    <FormControl type="text" value={this.state.usuario} placeholder="Enter user" name="usuario" onChange={this.handleChange} />
                    <FormControl.Feedback />
                    <Label>Password</Label>
                    <FormControl type="text" value={this.state.password} placeholder="Enter pass" name="password" onChange={this.handleChange} />
                    <Button type="submit" bsStyle="primary">  Enviar </Button>
                </FormGroup>
            </form>

        );
    }

}

export default Insert





// Agregar usuario //modulo axios para realizar peticiones
// axios.post('https://api-zendesk.herokuapp.com', {
        // //     usuario: 'unNombre',
        // //     password: '123'
        // // }).then((response) => {
        // //     console.log(response.data);

        // // }).catch(function (error) {
        // //     console.log(error);
        // // });