import React from 'react';
import { render } from 'react-dom';
//import ReactDOM from 'react-dom';
import App from './App';
import Insert from './Insert';
import './index.css';
import { Router, Route, hashHistory } from 'react-router'  //uso de rutas



// ReactDOM.render(
//   <App />,
//   document.getElementById('root')
// );

//Uso de rutas
render((
  <Router history={hashHistory}>
    <Route path="/" component={App} />
    <Route path="/insert" component={Insert} />
  </Router>
), document.getElementById('root'));


