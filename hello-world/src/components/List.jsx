import React from 'react';
import Button from 'react-bootstrap/lib/Button';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';
import FormControl from 'react-bootstrap/lib/FormControl';
import HelpBlock from 'react-bootstrap/lib/HelpBlock';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';
//PARA USAR REACT CON BOOTSTRATP INSTALAR MODULOS Y EN EL INDEX.HTML COPIAR EL CDN REACT-BOOTSTRAP, IMPORTAR LO QUE USAREMOS 

var List = React.createClass({
    render: function () {
        return (
            <div className="app">
                <div>Hi, react?! use React-Bootstrap</div>

                <ButtonToolbar>
                    {/* Standard button */}
                    <Button>Default</Button>

                    {/* Provides extra visual weight and identifies the primary action in a set of buttons */}
                    <Button bsStyle="primary">Primary</Button>

                    {/* Indicates a successful or positive action */}
                    <Button bsStyle="success">Success</Button>

                    {/* Contextual button for informational alert messages */}
                    <Button bsStyle="info">Info</Button>

                    {/* Indicates caution should be taken with this action */}
                    <Button bsStyle="warning">Warning</Button>

                    {/* Indicates a dangerous or potentially negative action */}
                    <Button bsStyle="danger">Danger</Button>

                    {/* Deemphasize a button by making it look like a link while maintaining button behavior */}
                    <Button bsStyle="link">Link</Button>
                </ButtonToolbar>

                <form>
                    <FormGroup
                        controlId="formBasicText">
                        <ControlLabel>Working example with validation</ControlLabel>
                        <FormControl
                            type="text"
                            placeholder="Enter text"
                            
                        />
                        <HelpBlock>Validation is based on string length.</HelpBlock>
                    </FormGroup>
                </form>

            </div>
        )
    }
});

module.exports = List;