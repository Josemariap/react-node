var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var methodOverride = require("method-override");
var axios = require("axios");  //libreria para realizar peticiones 
require('es6-promise').polyfill();
require('isomorphic-fetch');  //libreria para realizar peticiones
var controllers = require('./controllers/usercontrollers')

var app = express();

// Middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(bodyParser.json());
app.use(methodOverride());



var router = express.Router();

// getUsers
router.get('/users', controllers.getUsers)


app.use(router);

// Start server
app.listen(3000, function () {
    console.log("Node server running on http://localhost:3000");
});




//Peticion usando axios
// axios.get(`http://jsonplaceholder.typicode.com/users`)
//     .then(response => {
//         console.log(response.data)
//     }).catch(e => {
//         console.log(e)
//     }) 



