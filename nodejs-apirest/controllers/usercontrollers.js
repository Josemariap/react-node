
module.exports = {
    getUsers: function (req, res) {
        fetch('https://infinite-beach-97948.herokuapp.com/users')
            .then((response) => {
                console.log(response)
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                }
                return response.json();
            }).then((users) => {
                console.log(users)
                res.render('index', { users: users });
            })

    }
}

//fetch de ecma6