import React from 'react';
import ReactDOM from 'react-dom';
//import App from './App';

///////////////////////////////////////////////
// ReactDOM.render(
//   <App />,
//   document.getElementById('root')
// );




// var Component = React.createClass({
//   render: function () {
//     return (<h3>Descripcion sobre: {this.props.desc}</h3>)
//   }
// })

// ReactDOM.render(
//   <Component desc="muy corta la capital" />,
//   document.getElementById('componente')
// );

////////////////////////////////////////////////





////////////////////////////////////////////////


class User extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      usuario: '',
      password: '1234'
    };
    //this.cambiarEstadoUser = this.cambiarEstadoUser.bind(this); //Incorrecto
  }
  // cambiarEstadoUser(key, value) {   //Incorrecto
  //   this.setState({ [key]: value });
  // }
  componentWillMount() {
    console.log("Se ejecuta antes del render")
    this.setState({ usuario: "Dave" })
  }

  componentDidMount() {
    console.log("Se ejecuta despues del render")

  }
  handleClick = () => {
    console.log("Hace algo");
    var nada = 5;
  }
  render() {
    return (
      //this.cambiarEstadoUser("usuario", "Rose"), // Incorrecto
      console.log("Render" + this.props.time),
      <div>
        <h3>{this.state.usuario}</h3>
        <button name="btn1" onClick={this.handleClick}>
          Click me
        </button>
      </div>
    )
  }
}
//Definimos props, que no deben cambiar sos valores.
User.defaultProps = {
  initialCount: 0,
  time: 23
};

ReactDOM.render(
  <User />,
  document.getElementById('user')
);

//////////////////////////////////////////////







////////////////////////////////////////////////////////////////////////////////////////



/*function formatName(user) {
  return user.firstName + ' ' + user.lastName;
}

function getGreeting(user) {
  if (user) {
    return <h3>Hello, {formatName(user)}!</h3>;
  }
  return <h3>Hello, Stranger.</h3>;
}



const user = {
  firstName: 'Harper',
  lastName: 'Perez'
};

const element = (
  <h1>
    Hello, {getGreeting(user)}!
  </h1>
);


ReactDOM.render(
  element,
  document.getElementById('root2')
);

*/
////////////////////////////////////////////////////////




class LoginControl extends React.Component {
  constructor(props) {
    super(props);
    this.handleLoginClick = this.handleLoginClick.bind(this);
    this.handleLogoutClick = this.handleLogoutClick.bind(this);
    this.state = { isLoggedIn: false };
  }

  handleLoginClick() {
    this.setState({ isLoggedIn: true });
  }

  handleLogoutClick() {
    this.setState({ isLoggedIn: false });
  }

  render() {
    const isLoggedIn = this.state.isLoggedIn;

    let button = null;
    if (isLoggedIn) {
      button = <LogoutButton onClick={this.handleLogoutClick} />;
    } else {
      button = <LoginButton onClick={this.handleLoginClick} />;
    }

    return (
      <div>
        <Greeting isLoggedIn={isLoggedIn} />
        {button}
      </div>
    );
  }
}

function UserGreeting(props) {
  return <h1>Welcome back!</h1>;
}

function GuestGreeting(props) {
  return <h1>Please sign up.</h1>;
}

function Greeting(props) {
  const isLoggedIn = props.isLoggedIn;
  if (isLoggedIn) {
    return <UserGreeting />;
  }
  return <GuestGreeting />;
}

function LoginButton(props) {
  return (
    <button onClick={props.onClick}>
      Login
    </button>
  );
}

function LogoutButton(props) {
  return (
    <button onClick={props.onClick}>
      Logout
    </button>
  );
}

ReactDOM.render(
  <LoginControl />,
  document.getElementById('root2')
);


const numbers = [1, 2, 3, 4, 5, 6, 4, 2, 44, 2, 10];
const listItems = numbers.map((number, index) =>
  <li key={index}>
    {number}
  </li>
)

ReactDOM.render(
  <ul>{listItems}</ul>,
  document.getElementById('root')
);


////////////////////////////////////////




class ProductCategoryRow extends React.Component {
  render() {
    return <tr><th colSpan="2">{this.props.category}</th></tr>;
  }
}

class ProductRow extends React.Component {
  render() {
    var name = this.props.product.stocked ?
      this.props.product.name :
      <span style={{ color: 'red' }}>
        {this.props.product.name}
      </span>;
    return (
      <tr>
        <td>{name}</td>
        <td>{this.props.product.price}</td>
      </tr>
    );
  }
}

class ProductTable extends React.Component {
  render() {
    var rows = [];
    var lastCategory = null;
    this.props.products.forEach(function (product) {
      if (product.category !== lastCategory) {
        rows.push(<ProductCategoryRow category={product.category} key={product.category} />);
      }
      rows.push(<ProductRow product={product} key={product.name} />);
      lastCategory = product.category;
    });
    return (
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>{rows}</tbody>
      </table>
    );
  }
}




class SearchBar extends React.Component {
  render() {
    return (
      <form>
        <input type="text" placeholder="Search..." />
        <p>
          <input type="checkbox" />
          {' '}
          Only show products in stock
        </p>
      </form>
    );
  }
}


class FilterableProductTable extends React.Component {
  render() {
    return (
      <div>
        <SearchBar />
        <ProductTable products={this.props.products} />
      </div>
    );
  }
}


var PRODUCTS = [
  { category: 'Sporting Goods', price: '$49.99', stocked: true, name: 'Football' },
  { category: 'Sporting Goods', price: '$9.99', stocked: true, name: 'Baseball' },
  { category: 'Sporting Goods', price: '$29.99', stocked: false, name: 'Basketball' },
  { category: 'Electronics', price: '$99.99', stocked: true, name: 'iPod Touch' },
  { category: 'Electronics', price: '$399.99', stocked: false, name: 'iPhone 5' },
  { category: 'Electronics', price: '$199.99', stocked: true, name: 'Nexus 7' }
];

ReactDOM.render(
  <FilterableProductTable products={PRODUCTS} />,
  document.getElementById('root2')
)










